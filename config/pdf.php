<?php

return [
	'mode'                  => 'utf-8',
	'format'                => [256,177],//A4, A4-L, [1024,709], [256,177] -- default
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'margin-header'			=> '0',
	'creator'               => 'prasenjit.aluni@gmail.com',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('storage/mpdf/')
];
