<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyInternalmemberRequest;
use App\Http\Requests\StoreInternalmemberRequest;
use App\Http\Requests\UpdateInternalmemberRequest;
use App\Internalmember;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class InternalmemberController extends Controller
{
    use CsvImportTrait;

    public function index()
    {
        abort_if(Gate::denies('internalmember_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $internalmembers = Internalmember::all();

        return view('admin.internalmembers.index', compact('internalmembers'));
    }

    public function create()
    {
        abort_if(Gate::denies('internalmember_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.internalmembers.create');
    }

    public function store(StoreInternalmemberRequest $request)
    {
        $internalmember = Internalmember::create($request->all());

        return redirect()->route('admin.internalmembers.index');
    }

    public function edit(Internalmember $internalmember)
    {
        abort_if(Gate::denies('internalmember_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.internalmembers.edit', compact('internalmember'));
    }

    public function update(UpdateInternalmemberRequest $request, Internalmember $internalmember)
    {
        $internalmember->update($request->all());

        return redirect()->route('admin.internalmembers.index');
    }

    public function show(Internalmember $internalmember)
    {
        abort_if(Gate::denies('internalmember_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.internalmembers.show', compact('internalmember'));
    }

    public function destroy(Internalmember $internalmember)
    {
        abort_if(Gate::denies('internalmember_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $internalmember->delete();

        return back();
    }

    public function massDestroy(MassDestroyInternalmemberRequest $request)
    {
        Internalmember::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
