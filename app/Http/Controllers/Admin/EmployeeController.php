<?php
namespace App\Http\Controllers\Admin;
use App\Certificate;
use App\Employee;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyEmployeeRequest;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Internalmember;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use PDF;
use Auth;
use Libern\QRCodeReader\QRCodeReader;
use Zxing\QrReader;

class EmployeeController extends Controller
{
    use CsvImportTrait;
	
	public function democode(){

		$img = "/images/qrcode/qrcode-1-0-1-27-06-2020-14-50-35.png";
		//echo '<img src="'.$img.'" width="100"    />';QrCode
		
	
$qrcode = new Zxing\QrReader($img);
$text 	= $qrcode->text();
echo $text;
exit;exit;
	/*
		$QRCodeReader = new Libern\QRCodeReader\QRCodeReader();
		$qrcode_text = $QRCodeReader->decode("/images/qrcode/qrcode-1-0-1-27-06-2020-14-50-35.png");
		echo $qrcode_text;		
		dd(555);*/
		
		return view('/admin/employees/democode');
	}
	
	public function allmember(Request $request){
		abort_if(Gate::denies('employee_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		//dd($certificate);
        $employees = Employee::get();
		
		if(Auth::user()->roles[0]->title == 'Admin'){
			$employees = Employee::get();
		}
		elseif(Auth::user()->roles[0]->title == 'Officer'){
			$employees = Employee::where('created_by',Auth::user()->id)->get();
		}
		else{			
			$employees = Employee::where('department',Auth::user()->department)->get();
		}
		
		
        $users = User::all();
		$usersname = array();
		foreach($users as $val){
			$usersname[$val->id] = $val->name;
		}
		$type = 3;
		$mass_actiontxt = '';
		if(Auth::user()->roles[0]->title == 'Manager'){
			$mass_actiontxt = 'Reviewed';
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$mass_actiontxt = 'Approved';
		}
        return view('admin.employees.external.index', compact('employees','certificate','usersname','type','mass_actiontxt'));
	}
	
    public function index(Request $request)
    {
        abort_if(Gate::denies('employee_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		
		/*
		$type = 'external';
		if($request->type == 'internal'){
			$type = 'internal';
		}
		*/
				$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		//dd($certificate);
		
		$employees = array();
		$whr = array();
        $whereIn = array();
		$whr['employee_type'] = 2;
		if(Auth::user()->roles[0]->title == 'Admin'){
			$employees = Employee::where($whr)->get();
		}
		elseif(Auth::user()->roles[0]->title == 'Officer'){
			$whr['created_by']		= Auth::user()->id;
			$whr['certificate_approval_status']	= 1;
			$employees = Employee::where($whr)->get();
			//$employees = Employee::where('employee_type',2)->where('created_by',Auth::user()->id)->get();
		}
		elseif(Auth::user()->roles[0]->title == 'Manager'){
			$whr['department']		= Auth::user()->department;
			$employees = Employee::where($whr)->whereIn('certificate_approval_status',[1,4])->get();
			//dd($employees);
			//$employees = Employee::where('employee_type',2)->where('department',Auth::user()->department)->get();
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$whr['employee_type'] 	= 2;
			$whr['department']		= Auth::user()->department;
			$whr['certificate_approval_status']	= 4;
			$employees = Employee::where($whr)->get();
			//$employees = Employee::where('employee_type',2)->where('department',Auth::user()->department)->get();
		}
		//dd($employees);
		
		
		
        $users = User::all();
		$usersname = array();
		foreach($users as $val){
			$usersname[$val->id] = $val->name;
		}
		$type = 2;
		$mass_actiontxt = '';
		if(Auth::user()->roles[0]->title == 'Manager'){
			$mass_actiontxt = 'Reviewed';
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$mass_actiontxt = 'Approved';
		}
		//dd($employees);
		
        return view('admin.employees.external.index', compact('employees','certificate','usersname','type','mass_actiontxt'));
		
		
		
		/*
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		//dd($certificate);
        //$employees = Employee::get();

		
		
        $users = User::all();
		$usersname = array();
		foreach($users as $val){
			$usersname[$val->id] = $val->name;
		}
		$type = 2;
		$mass_actiontxt = '';
		if(Auth::user()->roles[0]->title == 'Manager'){
			$mass_actiontxt = 'Reviewed';
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$mass_actiontxt = 'Approved';
		}
		//dd($employees);
        return view('admin.employees.external.index', compact('employees','certificate','usersname','type','mass_actiontxt'));
		*/
    }

    public function create()
    {
        abort_if(Gate::denies('employee_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		//dd(Auth::user()->roles[0]->title);
		if(Auth::user()->roles[0]->title == 'Admin'){
			$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		}
		else{
			$res = Certificate::select('id','certificate_title','department')->where('department',Auth::user()->department)->get()->toArray();
		}
		
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] = $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		$type = 2;
        return view('admin.employees.external.create', compact('certificate','type'));
    }

    public function store(StoreEmployeeRequest $request)
    {
		//dd($request->employee_type);
        $employee = Employee::create($request->all());

		if($request->employee_type == 1){
			return redirect()->route('admin.members.index');
		}
		else{
			return redirect()->route('admin.employees.index');
		}		        
    }

    public function edit(Employee $employee)
    {
        abort_if(Gate::denies('employee_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] = $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
        return view('admin.employees.external.edit', compact('employee','certificate'));
    }

    public function update(UpdateEmployeeRequest $request, Employee $employee)
    {
        $employee->update($request->all());
		if($request->employee_type == 1){
			return redirect()->route('admin.members.index');
		}
		else{
			return redirect()->route('admin.employees.index');
		}        
    }

    public function show(Employee $employee)
    {
        abort_if(Gate::denies('employee_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.employees.external.show', compact('employee'));
    }

    public function destroy(Employee $employee)
    {
        abort_if(Gate::denies('employee_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $employee->delete();

        return back();
    }

    public function massDestroy(MassDestroyEmployeeRequest $request)
    {
		//print_r($request->ids);
        
		
		if(Auth::user()->roles[0]->title == 'HOD'){
			$updatedata = array('certificate_approve_by'=>Auth::user()->id,'certificate_approval_status'=>2);
			Employee::whereIn('id', request('ids'))->where('certificate_approval_status',4)->update($updatedata);
		}
		elseif(Auth::user()->roles[0]->title == 'Manager'){			
			$updatedata = array('reviewed_by'=>Auth::user()->id,'certificate_approval_status'=>4);
			Employee::whereIn('id', request('ids'))->where('certificate_approval_status',1)->update($updatedata);
		}
		//print_r($updatedata);
		
		//exit;
		//Employee::whereIn('id', request('ids'))->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }
	
	public function certificateapprove(Request $request){		
		if(Auth::user()->roles[0]->title == 'HOD'){
			$empid = $request->empid;
			$employee 	= Employee::where('id',$empid)->get();			
			if(isset($employee[0]->certificate_approval_status) && $employee[0]->certificate_approval_status == 4){				
				Employee::where('id',$empid)->update(array('certificate_approval_status'=>2,'certificate_approve_by'=>Auth::user()->id));

				if($employee[0]->employee_type == 2){
					return redirect('admin/employees');
				}
				else{
					return redirect('admin/members');
				}				
			}
		}		
		return redirect('admin/members');
	}
	
	public function certificatereview(Request $request){
		//dd(88);
		if(Auth::user()->roles[0]->title == 'Manager'){
			$empid 		= $request->empid;
			$employee 	= Employee::where('id',$empid)->get();
			if(isset($employee[0]->certificate_approval_status) && $employee[0]->certificate_approval_status == 1){
				$empid = $request->empid;
				Employee::where('id',$empid)->update(array('certificate_approval_status'=>4,'reviewed_by'=>Auth::user()->id));
				if($employee[0]->employee_type == 2){
					return redirect('admin/employees');
				}
				else{
					return redirect('admin/members');
				}					
			}
		}		
		return redirect('admin/members');
	}
	
	public function certificatepdf(Request $request){
		$user			= array();
		$empid 			= $request->empid;
		$employee 		= Employee::where('id',$empid)->get()->toArray();
		$certificate 	= Certificate::where('id',$employee[0]['default_certificate'])->get()->toArray();
		$res 			= User::get()->toArray();
		foreach($res as $val){
			$user[$val['id']] = $val;
		}
		//dd($user);
		
		
		if($employee[0]['emp_id'] == ''){
			$employee[0]['emp_id'] = 0;
		}
		

		$qrstr = "";
		//$qrstr = $certificate[0]['certificate_title'];
		$qrstr .= 'CertificateId-'.$employee[0]['id'];
		if($employee[0]['employee_type'] == 1){
			$qrstr .= '-Internal';
		}
		else{
			$qrstr .= '-External';
		}
		$qrstr .= '-'.$employee[0]['first_name'];
		$qrstr .= '-'.$employee[0]['last_name'];
		$qrstr .= '-'.$employee[0]['department'];
		if($employee[0]['issue_date'] != NULL){
			$qrstr .= '-'.$employee[0]['issue_date'];
		}		
		if($employee[0]['issue_date'] != NULL){
			$qrstr .= '-'.$employee[0]['serial_no'];
		}
		
		
		
		//dd($qrstr,$employee[0],$certificate);
		
		
		
		//echo $qrstr = $employee[0]['id'].'-'.$employee[0]['emp_id'].'-'.$employee[0]['default_certificate'].'-'.date('d-m-Y-H-i-s');		
		
		//exit;
		//\QrCode::size(500)->format('png')->color(32,168,0)->backgroundColor(32,168,216)->merge('images/laravel.jpg', 0.5, true)->size(500)->errorCorrection('H')->generate('Certificate'.$qrstr, public_path('images/qrcode/qrcode-'.$qrstr.'.png'));		
		//\QrCode::size(500)->format('png')->color(183,133,55)->merge('images/laravel.jpg', 0.5, true)->size(500)->errorCorrection('H')->generate('Certificate'.$qrstr, public_path('images/qrcode/qrcode-'.$qrstr.'.png'));		
		/*
		\QrCode::size(500)->format('png')
		->color(183,133,55)
		->merge('images/laravel.jpg', 0.5, true)
		->size(500)
		->errorCorrection('H')
		->generate('Certificate'.$qrstr, public_path('images/qrcode/qrcode-'.$qrstr.'.png'));	*/	
		
		
		\QrCode::size(500)->format('png')
		->color(183,133,55)
		->merge('images/laravel.jpg', 0.2, true)
		->size(800)
		->errorCorrection('H')
		->generate($qrstr, public_path('images/qrcode/qrcode-'.$qrstr.'.png'));		
		
			
		
		$data = [
			'employee' 		=> $employee[0],
			'certificate' 	=> $certificate[0],
			'user' 			=> $user,
			'qrstr'			=> $qrstr
		];
		
		//dd($data);
		
		//$pdf = PDF::loadView('admin/employees/myPDF', $data, ['mode' => 'utf-8', 'format' => 'A4-L']);
		$pdf = PDF::loadView('admin/employees/myPDF', $data, ['mode' => 'utf-8','orientation' => 'L','format' => [335, 232]]);
		//$pdf = PDF::loadView('admin/employees/myPDF', $data, ['mode' => 'utf-8','orientation' => 'L','format' =>'Legal']);
		return $pdf->stream('certificate.pdf');
				
		
		
		
		$empid = $request->empid;
		//Employee::where('id',$empid)->update(array('certificate_approval_status'=>'Approved'));
		$emp = Employee::where('id',$empid)->get()->toArray();
		$empName = $emp[0]['first_name'].' '.$emp[0]['last_name'];
        $data = ['title' => 'Welcome to DOM PDF'];
        $pdf = PDF::loadView('admin/employees/myPDF', $data);
        return $pdf->download('certificate.pdf');		
		$msg = "Please wait certificate of $empName will be download .";
		return redirect('admin/employees')->with('success',$msg);;
	}
	
	public function listing(){
		abort_if(Gate::denies('employee_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		//dd($certificate);
        $employees = Employee::get();
		
		if(Auth::user()->roles[0]->title == 'Admin'){
			$employees = Employee::get();
		}
		elseif(Auth::user()->roles[0]->title == 'Officer'){
			$employees = Employee::where('created_by',Auth::user()->id)->get();
		}
		else{			
			$employees = Employee::where('department',Auth::user()->department)->get();
		}
		
		
        $users = User::all();
		$usersname = array();
		foreach($users as $val){
			$usersname[$val->id] = $val->name;
		}
		$type = 4;
		$mass_actiontxt = '';
		if(Auth::user()->roles[0]->title == 'Manager'){
			$mass_actiontxt = 'Reviewed';
		}
		elseif(Auth::user()->roles[0]->title == 'HOD'){
			$mass_actiontxt = 'Approved';
		}
        return view('admin.employees.external.index', compact('employees','certificate','usersname','type','mass_actiontxt'));
	}
	
	public function addcertificate(Request $request){
		$id 			= $request->id;
		$certificate	= $request->default_certificate;
		
		$member 		= Internalmember::where('id',$id)->get()->toArray();
		if(isset($member[0])){
			$data["default_certificate"]	= $certificate;
			$data["emp_id"] 			= $member[0]["emp_id"];
			$data["emp_category"] 		= $member[0]["emp_category"];
			$data["first_name"] 		= $member[0]["first_name"];
			$data["last_name"] 			= $member[0]["last_name"];
			$data["institution_name"] 	= $member[0]["institution_name"];
			$data["emailid"] 			= $member[0]["emailid"];
			$data["department"] 		= $member[0]["department"];
			$data["issue_date"] 		= $request->issue_date;
			$data["serial_no"] 			= $request->serial_no;;
			$data["created_by"] 		= Auth::user()->id;
			Employee::insert($data);
		}
		return redirect('/admin/members')->with('success','Added successfully');;;
	}

}
