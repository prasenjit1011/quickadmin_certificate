<?php

namespace App\Http\Controllers\Admin;
use App\Certificate;
use App\Employee;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyEmployeeRequest;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Internalmember;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use PDF;
use Auth;


class EmployeeinternalController extends Controller
{
    use CsvImportTrait;

	public function empSearch(Request $request){
		//dd($_POST);
		$emp_id 		= '';
		$employee 		= array();
		$certificate 	= array();
		if(isset($_POST['emp_id'])){
			$emp_id 	= $_POST['emp_id'];
			$employee	= Internalmember::where('emp_id',$emp_id)->get();//->toArray();
			
			if(isset($employee[0])){
				$employee	= $employee[0];
			}
			
			//dd($employee);
			
			
			$res = Certificate::select('id','certificate_title','department')->get()->toArray();
			
			foreach($res as $val){
				$certificate[$val['id']]['id'] = $val['id'];
				$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
				$certificate[$val['id']]['department'] 			= $val['department'];
			}			
		}
		//dd($emp_id);
		return view('admin.employees.internal.search',compact('emp_id','employee','certificate'));
	}
	
	
	
	
	
	
	
	
    public function index()
    {
        abort_if(Gate::denies('employee_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] 	= $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
		//dd($certificate);
        $employees = Employee::get();
		
		if(Auth::user()->roles[0]->title == 'Admin'){
			$employees = Employee::get();
		}
		elseif(Auth::user()->roles[0]->title == 'Agent'){
			$employees = Employee::where('created_by',Auth::user()->id)->get();
		}
		else{
			$employees = Employee::where('department',Auth::user()->department)->get();
		}
		
		
        $users = User::all();
		$usersname = array();
		foreach($users as $val){
			$usersname[$val->id] = $val->name;
		}
        return view('admin.employees.internal_index', compact('employees','certificate','usersname'));
    }

    public function create()
    {
        abort_if(Gate::denies('employee_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		
		if(Auth::user()->roles[0]->title == 'Admin'){
			$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		}
		else{
			$res = Certificate::select('id','certificate_title','department')->where('department',Auth::user()->roles[0]->title)->get()->toArray();
		}
		
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] = $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
        return view('admin.employees.create', compact('certificate'));
    }

    public function store(StoreEmployeeRequest $request)
    {
		//dd($request->all());
        $employee = Employee::create($request->all());

        return redirect()->route('admin.employees.index');
    }

    public function edit(Employee $employee)
    {
        abort_if(Gate::denies('employee_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$certificate = array();
		$res = Certificate::select('id','certificate_title','department')->get()->toArray();
		
		foreach($res as $val){
			$certificate[$val['id']]['id'] = $val['id'];
			$certificate[$val['id']]['certificate_title'] = $val['certificate_title'];
			$certificate[$val['id']]['department'] 			= $val['department'];
		}
        return view('admin.employees.edit', compact('employee','certificate'));
    }

    public function update(UpdateEmployeeRequest $request, Employee $employee)
    {
        $employee->update($request->all());

        return redirect()->route('admin.employees.index');
    }
 
    public function show(Employee $employee)
    {
        abort_if(Gate::denies('employee_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.employees.show', compact('employee'));
    }

    public function destroy(Employee $employee)
    {
        abort_if(Gate::denies('employee_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $employee->delete();

        return back();
    }

    public function massDestroy(MassDestroyEmployeeRequest $request)
    {
        Employee::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
	
	public function certificateapprove(Request $request){		
		if(Auth::user()->roles[0]->title == 'Manager'){
			$empid = $request->empid;
			$employee 	= Employee::where('id',$empid)->get();			
			if(isset($employee[0]->certificate_approval_status) && $employee[0]->certificate_approval_status == 4){				
				Employee::where('id',$empid)->update(array('certificate_approval_status'=>2,'certificate_approve_by'=>Auth::user()->id));
			}
		}
		return redirect('admin/employees');
	}
	
	public function certificatereview(Request $request){
		if(Auth::user()->roles[0]->title == 'Officer'){
			$empid 		= $request->empid;
			$employee 	= Employee::where('id',$empid)->get();
			if(isset($employee[0]->certificate_approval_status) && $employee[0]->certificate_approval_status == 1){
				$empid = $request->empid;
				Employee::where('id',$empid)->update(array('certificate_approval_status'=>4,'reviewed_by'=>Auth::user()->id));
			}
		}
		return redirect('admin/employees');
	}
	
	public function certificatepdf(Request $request){
		$user			= array();
		$empid 			= $request->empid;
		$employee 		= Employee::where('id',$empid)->get()->toArray();
		$certificate 	= Certificate::where('id',$employee[0]['default_certificate'])->get()->toArray();
		$res 			= User::get()->toArray();
		foreach($res as $val){
			$user[$val['id']] = $val;
		}
		
		$qrstr = $employee[0]['id'].'-'.$employee[0]['emp_id'].'-'.$employee[0]['default_certificate'];
		\QrCode::size(500)->format('png')->generate('Certificate'.$qrstr, public_path('images/qrcode/qrcode-'.$qrstr.'.png'));
		
		$data = [
			'employee' 		=> $employee[0],
			'certificate' 	=> $certificate[0],
			'user' 			=> $user,
			'qrstr'			=> $qrstr
		];
		
		
		
		//$pdf = PDF::loadView('admin/employees/myPDF', $data, ['mode' => 'utf-8', 'format' => 'A4-L']);
		$pdf = PDF::loadView('admin/employees/myPDF', $data, ['mode' => 'utf-8','orientation' => 'L','format' => [335, 232]]);
		//$pdf = PDF::loadView('admin/employees/myPDF', $data, ['mode' => 'utf-8','orientation' => 'L','format' =>'Legal']);
		return $pdf->stream('certificate.pdf');
				
		
		
		
		$empid = $request->empid;
		//Employee::where('id',$empid)->update(array('certificate_approval_status'=>'Approved'));
		$emp = Employee::where('id',$empid)->get()->toArray();
		$empName = $emp[0]['first_name'].' '.$emp[0]['last_name'];
        $data = ['title' => 'Welcome to DOM PDF'];
        $pdf = PDF::loadView('admin/employees/myPDF', $data);
        return $pdf->download('certificate.pdf');		
		$msg = "Please wait certificate of $empName will be download .";
		return redirect('admin/employees')->with('success',$msg);;
	}
}
