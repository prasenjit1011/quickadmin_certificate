@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.employee.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.employees.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.id') }}
                        </th>
                        <td>
                            {{ $employee->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.emp_category') }}
                        </th>
                        <td>
                            {{ App\Employee::EMP_CATEGORY_SELECT[$employee->emp_category] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.first_name') }}
                        </th>
                        <td>
                            {{ $employee->first_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.last_name') }}
                        </th>
                        <td>
                            {{ $employee->last_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.institution_name') }}
                        </th>
                        <td>
                            {{ $employee->institution_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.certificate_approval_status') }}
                        </th>
                        <td>
                            {{ App\Employee::CERTIFICATE_APPROVAL_STATUS_SELECT[$employee->certificate_approval_status] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.certificate_approve_by') }}
                        </th>
                        <td>
                            {{ $employee->certificate_approve_by }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.certificate_qrcode') }}
                        </th>
                        <td>
                            {{ $employee->certificate_qrcode }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.certificate_approve_date') }}
                        </th>
                        <td>
                            {{ $employee->certificate_approve_date }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.employee.fields.default_certificate') }}
                        </th>
                        <td>
                            {{ $employee->default_certificate }}
                        </td>
                    </tr>                    
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.employees.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection