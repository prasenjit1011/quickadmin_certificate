@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.internalmember.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.internalmembers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.internalmember.fields.id') }}
                        </th>
                        <td>
                            {{ $internalmember->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.internalmember.fields.emp_id') }}
                        </th>
                        <td>
                            {{ $internalmember->emp_id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.internalmember.fields.emp_category') }}
                        </th>
                        <td>
                            {{ App\Internalmember::EMP_CATEGORY_SELECT[$internalmember->emp_category] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.internalmember.fields.first_name') }}
                        </th>
                        <td>
                            {{ $internalmember->first_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.internalmember.fields.last_name') }}
                        </th>
                        <td>
                            {{ $internalmember->last_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.internalmember.fields.institution_name') }}
                        </th>
                        <td>
                            {{ $internalmember->institution_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.internalmember.fields.emailid') }}
                        </th>
                        <td>
                            {{ $internalmember->emailid }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.internalmember.fields.contact_no') }}
                        </th>
                        <td>
                            {{ $internalmember->contact_no }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.internalmember.fields.department') }}
                        </th>
                        <td>
                            {{ $internalmember->department }}
                        </td>
                    </tr>
                    
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.internalmembers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection