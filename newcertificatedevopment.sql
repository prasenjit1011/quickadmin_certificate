-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2020 at 05:23 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newcertificatedevopment`
--

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` int(11) NOT NULL DEFAULT 1,
  `certificate_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificate_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `template`, `certificate_title`, `certificate_subtitle`, `certificate_details`, `department`, `created_by`, `empid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'شــــــــــهــادة تــكـــــريـــــم', 'تتقدم الإدارة العامة للدفاع المدني - دبي بخالص الشكر والتقدير إلى', '<p>تقديرا لجهوده الفعالة في تقديم اقتراحا مجديا يسهم في تطوير العمل بما يحقق الأهداف الاستراتيجيةمع تمنياتنا له بالتوفيق والنجاح.</p>', 'EngDept', 1, 0, '2020-06-14 21:35:43', '2020-06-26 21:48:48', NULL),
(12, 1, 'Certificate Of Introduction', 'New Semester Certificate Of Introduction From UAE College', NULL, 'MedDept', 1, 0, '2020-06-26 23:02:53', '2020-06-26 23:02:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'EngDept', '2020-06-15 18:30:00', '2020-06-25 23:37:53', NULL),
(2, 'MedDept', '2020-06-25 23:29:22', '2020-06-25 23:37:40', NULL),
(3, 'CvilManager', '2020-06-25 23:38:14', '2020-06-25 23:38:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `reviewed_by` int(11) NOT NULL DEFAULT 0,
  `institution_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_certificate` int(11) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `serial_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_by` int(11) DEFAULT 0,
  `certificate_qrcode` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_date` date DEFAULT NULL,
  `employee_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `certificate_approval_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `emp_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `emp_id`, `emailid`, `contact_no`, `first_name`, `last_name`, `created_by`, `reviewed_by`, `institution_name`, `department`, `default_certificate`, `issue_date`, `serial_no`, `certificate_approve_by`, `certificate_qrcode`, `certificate_approve_date`, `employee_type`, `certificate_approval_status`, `emp_category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'prasenjit.aluni@gmail.com', NULL, 'Prasenjittt', 'Aluni', 19, 18, 'United String Technology', 'EngDept', 1, NULL, NULL, 15, NULL, NULL, '2', '2', '1', '2020-06-25 09:52:00', '2020-06-25 11:48:32', NULL),
(2, 'AR1452985', 'prasenjiluni@gmail.com', NULL, 'Sanjay', 'Aluni', 19, 0, 'Institute of Eng College', 'EngDept', 1, NULL, NULL, 0, NULL, NULL, '1', '1', '1', NULL, NULL, NULL),
(3, 'AR14529899', 'prasenjiluni@gmail.com', NULL, 'Sanjo', 'Aluni', 19, 0, 'Institute of Eng College', 'EngDept', 1, NULL, NULL, 0, NULL, NULL, '1', '1', '1', NULL, NULL, NULL),
(4, NULL, 'prasen.aluni@gmail.com', NULL, 'Prasenjit', 'Aluni', 1, 0, 'United String Technology', 'zsdsa', 1, '2020-06-17', 'dxcgdfgdvvvvv', 0, NULL, NULL, '2', '1', '1', '2020-06-26 23:43:01', '2020-06-26 23:43:51', NULL),
(8, 'AR1452985', 'prasenjiluni@gmail.com', NULL, 'Sanjay', 'Aluni', 1, 0, 'Institute of Eng College', 'EngDept', 1, '2020-06-12', 'Test123', 0, NULL, NULL, '1', '1', '1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `internalmembers`
--

CREATE TABLE `internalmembers` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institution_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approval_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_by` int(11) DEFAULT NULL,
  `certificate_approve_date` date DEFAULT current_timestamp(),
  `certificate_qrcode` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_certificate` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `employee_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `internalmembers`
--

INSERT INTO `internalmembers` (`id`, `emp_id`, `emp_category`, `first_name`, `last_name`, `institution_name`, `emailid`, `contact_no`, `department`, `certificate_approval_status`, `certificate_approve_by`, `certificate_approve_date`, `certificate_qrcode`, `default_certificate`, `created_by`, `employee_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'ENN4', '1', 'Jhon', 'Cronie', 'Institute of Eng College', 'uptickspo@gmail.com', '35345345', 'EngDept', NULL, NULL, '2020-06-25', NULL, NULL, NULL, NULL, '2020-06-25 09:57:39', '2020-06-25 10:18:26', NULL),
(6, 'DRE7', '4', 'Riyann', 'Cronieee', 'Institute of Medical College', 'upticko@gmail.com', '35345345', 'EngDept', NULL, NULL, '2020-06-25', NULL, NULL, NULL, NULL, '2020-06-25 10:00:24', '2020-06-25 10:15:07', NULL),
(7, 'AR1452985', '1', 'Sanjay', 'Aluni', 'Institute of Eng College', 'prasenjiluni@gmail.com', '35345345', 'EngDept', NULL, NULL, '2020-06-25', NULL, NULL, NULL, NULL, '2020-06-25 10:17:54', '2020-06-25 10:40:38', NULL),
(8, 'AR145298532', '5', 'Raman', 'RA', 'Institute of Medical College', 'prasenjit.aluni@gmail.com', '35345345', 'EngDept', NULL, NULL, '2020-06-25', NULL, NULL, NULL, NULL, '2020-06-25 10:45:57', '2020-06-25 10:45:57', NULL),
(9, 'AR145244', '1', 'Sumit', 'Cronie', 'Institute of Medical College', 'prasenluni@gmail.com', '35345345', 'EngDept', NULL, NULL, '2020-06-25', NULL, NULL, NULL, NULL, '2020-06-25 10:55:23', '2020-06-25 10:55:23', NULL),
(10, 'AR14529899', '1', 'Sanjo', 'Aluni', 'Institute of Eng College', 'prasenjiluni@gmail.com', NULL, 'EngDept', NULL, NULL, '2020-06-25', NULL, NULL, 19, NULL, NULL, '2020-06-25 11:23:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\Certificate', 1, 'certificate_logo', '5ee6e584d78e9_united-arab-emirates-university-squarelogo', '5ee6e584d78e9_united-arab-emirates-university-squarelogo.png', 'image/png', 'public', 51211, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 1, '2020-06-14 21:35:43', '2020-06-14 21:35:44'),
(2, 'App\\User', 2, 'signature', '5ee6ed6b3d9a4_signature', '5ee6ed6b3d9a4_signature.png', 'image/png', 'public', 1543, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 2, '2020-06-14 22:09:26', '2020-06-14 22:09:26'),
(4, 'App\\Certificate', 2, 'certificate_logo', '5eecd5dd985e7_ulogo', '5eecd5dd985e7_ulogo.jpg', 'image/jpeg', 'public', 31371, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 3, '2020-06-19 09:42:50', '2020-06-19 09:42:51'),
(5, 'App\\Certificate', 2, 'certificate_logo', '5eecd6003ea50_profile', '5eecd6003ea50_profile.png', 'image/png', 'public', 10350, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 4, '2020-06-19 09:43:25', '2020-06-19 09:43:26'),
(6, 'App\\Certificate', 8, 'certificate_logo', '5eed79da6a549_36397354_1044702075693358_2807173728264257536_n (1)', '5eed79da6a549_36397354_1044702075693358_2807173728264257536_n-(1).jpg', 'image/jpeg', 'public', 161371, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 5, '2020-06-19 21:23:17', '2020-06-19 21:23:28'),
(7, 'App\\Certificate', 8, 'certificate_logo', '5eed7a589531e_36466278_1044699755693590_3334984831955107840_n', '5eed7a589531e_36466278_1044699755693590_3334984831955107840_n.jpg', 'image/jpeg', 'public', 91124, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 6, '2020-06-19 21:24:18', '2020-06-19 21:24:19'),
(8, 'App\\Certificate', 8, 'certificate_logo', '5eed7a8100dac_36426773_1044701452360087_8121610675144359936_n', '5eed7a8100dac_36426773_1044701452360087_8121610675144359936_n.jpg', 'image/jpeg', 'public', 79744, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 7, '2020-06-19 21:24:58', '2020-06-19 21:24:59'),
(9, 'App\\Certificate', 8, 'certificate_logo', '5eed7ad7b1b5e_36426773_1044701452360087_8121610675144359936_n', '5eed7ad7b1b5e_36426773_1044701452360087_8121610675144359936_n.jpg', 'image/jpeg', 'public', 79744, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 8, '2020-06-19 21:26:25', '2020-06-19 21:26:26'),
(10, 'App\\Certificate', 8, 'certificate_logo', '5eed7be2d91ab_36426773_1044701452360087_8121610675144359936_n', '5eed7be2d91ab_36426773_1044701452360087_8121610675144359936_n.jpg', 'image/jpeg', 'public', 79744, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 9, '2020-06-19 21:30:52', '2020-06-19 21:30:53'),
(11, 'App\\Certificate', 8, 'certificate_logo', '5eed82973f8e7_36426773_1044701452360087_8121610675144359936_n', '5eed82973f8e7_36426773_1044701452360087_8121610675144359936_n.jpg', 'image/jpeg', 'public', 79744, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 10, '2020-06-19 21:59:30', '2020-06-19 21:59:31'),
(12, 'App\\Certificate', 8, 'certificate_logo', '5eed82b21f85f_35521614_1032592953570937_1321816197259329536_n', '5eed82b21f85f_35521614_1032592953570937_1321816197259329536_n.jpg', 'image/jpeg', 'public', 333252, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 11, '2020-06-19 21:59:55', '2020-06-19 21:59:56'),
(13, 'App\\Certificate', 8, 'certificate_signature', '5ef58d7f32b0c_logo-icon', '5ef58d7f32b0c_logo-icon.png', 'image/png', 'public', 930, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 12, '2020-06-26 00:24:09', '2020-06-26 00:24:20'),
(14, 'App\\Certificate', 9, 'certificate_logo', '5ef58e6bcf47a_chair', '5ef58e6bcf47a_chair.jpg', 'image/jpeg', 'public', 31299, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 13, '2020-06-26 00:28:09', '2020-06-26 00:28:10'),
(16, 'App\\Certificate', 9, 'certificate_logo', '5ef58e8c8d061_chair4', '5ef58e8c8d061_chair4.jpg', 'image/jpeg', 'public', 28041, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 15, '2020-06-26 00:28:44', '2020-06-26 00:28:45'),
(17, 'App\\Certificate', 1, 'certificate_logo', '5ef58fb04383d_certificate_logo', '5ef58fb04383d_certificate_logo.png', 'image/png', 'public', 44575, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 16, '2020-06-26 00:34:21', '2020-06-26 00:34:21'),
(18, 'App\\Certificate', 1, 'certificate_signature', '5ef58fe435686_signature', '5ef58fe435686_signature.jpeg', 'image/jpeg', 'public', 52973, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 17, '2020-06-26 00:34:21', '2020-06-26 00:34:22'),
(19, 'App\\Certificate', 10, 'certificate_logo', '5ef6c9be91245_DSC02256', '5ef6c9be91245_DSC02256.JPG', 'image/jpeg', 'public', 3714015, '[]', '[]', '[]', 18, '2020-06-26 22:53:49', '2020-06-26 22:53:49'),
(20, 'App\\Certificate', 10, 'certificate_logo', '5ef6ca78564ab_download (1)', '5ef6ca78564ab_download-(1).jpg', 'image/jpeg', 'public', 9956, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 19, '2020-06-26 22:56:39', '2020-06-26 22:56:43'),
(21, 'App\\Certificate', 11, 'certificate_logo', '5ef6cb1480a0b_download', '5ef6cb1480a0b_download.jpg', 'image/jpeg', 'public', 10995, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 20, '2020-06-26 22:59:16', '2020-06-26 22:59:17'),
(22, 'App\\Certificate', 11, 'certificate_signature', '5ef6cb1adade6_green-trees-leafs-jpg-168114194', '5ef6cb1adade6_green-trees-leafs-jpg-168114194.jpg', 'image/jpeg', 'public', 49191, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 21, '2020-06-26 22:59:17', '2020-06-26 22:59:17'),
(23, 'App\\Certificate', 12, 'certificate_logo', '5ef6cbef21f32_download (1)', '5ef6cbef21f32_download-(1).jpg', 'image/jpeg', 'public', 9956, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 22, '2020-06-26 23:02:54', '2020-06-26 23:02:54'),
(24, 'App\\Certificate', 12, 'certificate_signature', '5ef6cbf4b93b4_green-trees-leafs-jpg-168114194', '5ef6cbf4b93b4_green-trees-leafs-jpg-168114194.jpg', 'image/jpeg', 'public', 49191, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 23, '2020-06-26 23:02:54', '2020-06-26 23:02:55'),
(25, 'App\\Certificate', 12, 'stamp', '5ef6d3764bcc0_download', '5ef6d3764bcc0_download.jpg', 'image/jpeg', 'public', 10995, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 24, '2020-06-26 23:34:55', '2020-06-26 23:34:55');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institution_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approval_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_approve_by` int(11) DEFAULT NULL,
  `certificate_approve_date` date DEFAULT NULL,
  `certificate_qrcode` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_certificate` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `employee_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2020_06_15_000001_create_media_table', 1),
(8, '2020_06_15_000002_create_permissions_table', 1),
(9, '2020_06_15_000003_create_roles_table', 1),
(10, '2020_06_15_000004_create_users_table', 1),
(11, '2020_06_15_000005_create_employees_table', 1),
(12, '2020_06_15_000006_create_certificates_table', 1),
(13, '2020_06_15_000007_create_permission_role_pivot_table', 1),
(14, '2020_06_15_000008_create_role_user_pivot_table', 1),
(15, '2020_06_16_000007_create_departments_table', 2),
(16, '2020_06_18_000008_create_members_table', 3),
(17, '2020_06_25_000008_create_members_table', 4),
(18, '2020_06_25_000010_create_internalmembers_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', NULL, NULL, NULL),
(2, 'permission_create', NULL, NULL, NULL),
(3, 'permission_edit', NULL, NULL, NULL),
(4, 'permission_show', NULL, NULL, NULL),
(5, 'permission_delete', NULL, NULL, NULL),
(6, 'permission_access', NULL, NULL, NULL),
(7, 'role_create', NULL, NULL, NULL),
(8, 'role_edit', NULL, NULL, NULL),
(9, 'role_show', NULL, NULL, NULL),
(10, 'role_delete', NULL, NULL, NULL),
(11, 'role_access', NULL, NULL, NULL),
(12, 'user_create', NULL, NULL, NULL),
(13, 'user_edit', NULL, NULL, NULL),
(14, 'user_show', NULL, NULL, NULL),
(15, 'user_delete', NULL, NULL, NULL),
(16, 'user_access', NULL, NULL, NULL),
(17, 'employee_create', NULL, NULL, NULL),
(18, 'employee_edit', NULL, NULL, NULL),
(19, 'employee_show', NULL, NULL, NULL),
(20, 'employee_delete', NULL, NULL, NULL),
(21, 'employee_access', NULL, NULL, NULL),
(22, 'certificate_create', NULL, NULL, NULL),
(23, 'certificate_edit', NULL, NULL, NULL),
(24, 'certificate_show', NULL, NULL, NULL),
(25, 'certificate_delete', NULL, NULL, NULL),
(26, 'certificate_access', NULL, NULL, NULL),
(27, 'profile_password_edit', NULL, NULL, NULL),
(28, 'member_create', NULL, NULL, NULL),
(29, 'member_edit', NULL, NULL, NULL),
(30, 'member_show', NULL, NULL, NULL),
(31, 'member_delete', NULL, NULL, NULL),
(32, 'employee_access', NULL, NULL, NULL),
(33, 'internal_employee_listing', '2020-06-25 00:00:32', '2020-06-25 00:00:32', NULL),
(34, 'internalmember_access', '2020-06-25 01:39:30', '2020-06-25 01:39:30', NULL),
(35, 'internalmember_create', '2020-06-25 01:53:00', '2020-06-25 01:53:00', NULL),
(36, 'internalmember_edit', '2020-06-25 01:53:10', '2020-06-25 01:53:10', NULL),
(37, 'internalmember_delete', '2020-06-25 01:53:21', '2020-06-25 01:53:21', NULL),
(38, 'internalmember_show', '2020-06-25 01:53:34', '2020-06-25 01:53:34', NULL),
(39, 'department_access', '2020-06-25 23:16:41', '2020-06-25 23:27:56', NULL),
(40, 'department_create', '2020-06-25 23:17:20', '2020-06-25 23:29:00', NULL),
(41, 'department_edit', '2020-06-25 23:17:31', '2020-06-25 23:28:12', NULL),
(42, 'department_delete', '2020-06-25 23:17:43', '2020-06-25 23:28:24', NULL),
(43, 'department_show', '2020-06-25 23:17:55', '2020-06-25 23:27:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(3, 1),
(3, 12),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(3, 18),
(3, 19),
(3, 20),
(3, 21),
(3, 22),
(3, 23),
(3, 24),
(3, 25),
(3, 26),
(3, 27),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 21),
(4, 24),
(4, 26),
(4, 27),
(5, 17),
(5, 18),
(5, 19),
(5, 20),
(5, 21),
(5, 24),
(5, 26),
(5, 27),
(4, 14),
(4, 16),
(4, 12),
(4, 13),
(4, 15),
(4, 1),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(5, 28),
(5, 29),
(5, 30),
(5, 31),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(5, 33),
(5, 34),
(5, 35),
(5, 36),
(5, 37),
(5, 38),
(4, 33),
(4, 34),
(4, 35),
(4, 36),
(4, 37),
(4, 38),
(3, 33),
(3, 34),
(3, 35),
(3, 36),
(3, 37),
(3, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(6, 3),
(6, 4),
(6, 5),
(6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'User', NULL, NULL, NULL),
(3, 'HOD', '2020-06-14 22:12:30', '2020-06-18 23:52:07', NULL),
(4, 'Manager', '2020-06-16 00:31:00', '2020-06-18 23:52:23', NULL),
(5, 'Officer', '2020-06-16 01:39:37', '2020-06-18 23:52:39', NULL),
(6, 'Principal', '2020-06-27 04:13:16', '2020-06-27 04:13:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(15, 3),
(16, 3),
(17, 3),
(18, 4),
(19, 5),
(20, 5),
(21, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `created_by`, `name`, `signature_name`, `department`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'Admin', NULL, NULL, 'admin@admin.com', NULL, '$2y$10$KjXReev6r5pHvflCEqyPM.dZ2YvsPlbQA5EbDJEkd6vLkMR3O/QZi', 'Bgkke0w5qgAHJJtDL6nZZ8li6dgFbwPEasInmUN7b9fag6WZte8hVyiIMydF', NULL, '2020-06-26 00:02:09', NULL),
(15, 1, 'Eng HOD', 'نتاايب استيباتسيب انتسيابسي', 'EngDept', 'eng_hod@gmail.com', NULL, '$2y$10$Aj6ug4.oz3kZvbYVkU7HAuEKKrA7Ao5PMz.KbYU4Is3XEPH9bdfaG', '5Lpt39t7hQcs0zuWjKSDSylqzYKZWiKMO868QYtsw0Flbg2s9MmLGe7TDDjD', '2020-06-17 10:38:52', '2020-06-17 10:42:23', NULL),
(16, 1, 'Med HOD', 'استيباتسيب انتسيابسي', 'MedDept', 'med_hod@gmail.com', NULL, '$2y$10$98Xts18xdClRYVcXLdQ5FuL3BQxShCNRlYruLNrNA0cQh9IDCndjG', NULL, '2020-06-17 10:42:08', '2020-06-17 10:42:08', NULL),
(17, 1, 'Cvil HOD', 'استيباتسيب انتسيابسي', 'CvilManager', 'cvil_hod@gmail.com', NULL, '$2y$10$y9CcZH2.NrYy3krj1SBM9uhpIqrxSq09V857qcJTD9Z5wI.MN79Hy', 'AEAt03Yqlo77X1wfkzzIaQDwEF9wN6QPdphKTLZLsb3PN1IJPF9gMtgti7hF', '2020-06-17 10:43:47', '2020-06-17 10:43:47', NULL),
(18, 15, 'Eng Manager', NULL, 'EngDept', 'eng_manager@gmail.com', NULL, '$2y$10$q0kPA74ym2Z8914gWS41wOYxjF1dbN03jX5EU2FaeHAYFi4tag0wW', 'a2N9JUZ63aBOW6KRHJN4SPk23coQhxjuxrsPfXBdP13MR4pcYC6oIna1hItN', '2020-06-17 10:48:33', '2020-06-17 10:57:10', NULL),
(19, 18, 'Eng Officer', NULL, 'EngDept', 'eng_officer@gmail.com', NULL, '$2y$10$kx4rtDKRWLGDfheFlcTK1ul0Wr.np6.OekOnfFR2r.vJD3fmKNrJu', 'kmGBoUKI0ZxrTUTnl9FiYNEVLiUGmw6Km1c5FKOEgY9hMtHO0N19UhM5EaTb', '2020-06-17 10:55:53', '2020-06-17 10:55:53', NULL),
(20, 18, 'Eng Officer X', NULL, 'EngDept', 'eng_officerx@gmail.com', NULL, '$2y$10$I66Qap79Ql2TbEYIYuFHFOp9UbaLypmroLrS0VFJ7LkN3FVe0i5ze', '0b2mdjWxYMueSI1nvNWBCBXH2tmQiyFgtiaYSaFB6Jfr2u81riKNRm5gRGKy', '2020-06-17 11:01:33', '2020-06-17 11:01:33', NULL),
(21, 16, 'Med Manager', NULL, 'MedDept', 'med_manager@gmail.com', NULL, '$2y$10$lTidbwe2EqgxIejbk2LXseUpMbOecnR1Q8sBF7olJDF2K5PlvTfxm', NULL, '2020-06-17 12:09:03', '2020-06-17 12:09:03', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internalmembers`
--
ALTER TABLE `internalmembers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `role_id_fk_1645052` (`role_id`),
  ADD KEY `permission_id_fk_1645052` (`permission_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `user_id_fk_1645061` (`user_id`),
  ADD KEY `role_id_fk_1645061` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `internalmembers`
--
ALTER TABLE `internalmembers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_id_fk_1645052` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_id_fk_1645052` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_id_fk_1645061` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_id_fk_1645061` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
