<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$DDQVG2OqEtOuDLpkhtKsHuC3yg2/hXUE9umByObUhRlBzL3PlaYES',
                'remember_token' => null,
            ],
        ];

        User::insert($users);
    }
}
